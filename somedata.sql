SET FOREIGN_KEY_CHECKS = 0;
truncate table student;
truncate table studies;
truncate table department;
truncate table course;
truncate table semester;
truncate table course_run;
SET FOREIGN_KEY_CHECKS = 0;

insert into student values ('zitnyjak');
insert into studies (id, student_username) values (1, 'zitnyjak'); 
insert into department (department_id) values (1);
insert into course values ('BI-MLO', 'xx', 'xx', 5, 1);
insert into course values ('BI-AAG', 'xx', 'xx', 4, 1);
insert into course values ('BI-ZMA', 'xx', 'xx', 4, 1);
insert into semester (id) values (1);
insert into semester (id) values (2);
insert into course_run (id, semester_id, course_code) values (1, 1, 'BI-MLO');
insert into course_run (id, semester_id, course_code) values (2, 1, 'BI-AAG');
insert into course_run (id, semester_id, course_code) values (3, 2, 'BI-ZMA');

-- select from student join studies on student.username = studies.student_username where student.username = 'zitnyjak'
-- select * from student_in_course_runs where student.username = 'zitnyjak'
-- join course

