--
-- mysql target ddl
--

-- drop all tables, ignore the foreign key checks
SET FOREIGN_KEY_CHECKS = 0;
drop table if exists contact_details_version;
drop table if exists contact_details;
drop table if exists student;
drop table if exists studies;
drop table if exists students_in_course_runs;
drop table if exists course_run;
drop table if exists edux_details;
drop table if exists course;
drop table if exists department;
drop table if exists semester;
drop table if exists course_details;
drop table if exists course_details_version;
SET FOREIGN_KEY_CHECKS = 1;

-- create tables
create table contact_details_version (
  id varchar(255),
  street varchar(255),
  city varchar(255),
  zip varchar(20),
  country varchar(255),
  phone varchar(20),
  email varchar(20),
  version integer,
  primary key (id)
);

create table contact_details (
  id varchar(255),
  primary key (id)
);

create table student (
  username varchar(255),
  primary key (username)
);

create table studies (
  id varchar(255),
  study_major_code varchar(20),
  study_major_name varchar(20),
  study_form varchar(20),
  study_program varchar(20),
  is_studying varchar(1),
  finished_as varchar(1),
  start_date datetime,
  end_date datetime,
  primary key (id)
);

create table students_in_course_runs (
  id varchar(255),
  primary key (id)
);

create table course_run (
  id varchar(255),
  semester_completed tinyint,
  completed tinyint,
  semester_completed_date datetime,
  completed_date datetime,
  grade varchar(1),
  exam_takes integer,
  primary key (id)
);

create table edux_details (
  id varchar(255),
  semester_completed tinyint,
  grade varchar(1),
  points_semester double,
  points_semester_end double,
  points_exam double,
  points_part1 double,
  points_part2 double,
  points_part3 double,
  primary key (id)
);

create table course (
  course_code varchar(255),
  course_role varchar(50),
  completion_type varchar(20),
  credits integer,
  primary key (course_code)
);

create table department (
  department_id varchar(255),
  faculty_id varchar(20),
  `type` varchar(255),
  name_cs varchar(50),
  name_en varchar(50),
  shortname_cs varchar(20),
  shortname_en varchar(20),
  primary key (department_id)
);

create table semester (
  id varchar(255),
  name_cs varchar(50),
  name_en varchar(50),
  special_mode varchar(50),
  start_date datetime,
  end_date datetime,
  primary key (id)
);

create table course_details (
  id varchar(255),
  primary key (id)
);

create table course_details_version (
  id varchar(255),
  language varchar(3),
  course_name varchar(50),
  keywords text,
  annotations text,
  requirements text,
  goals text,
  summary text,
  summary_semester text,
  literature text,
  version integer,
  primary key (id)
);

-- add foregin keys
alter table contact_details_version
  add contact_details_id varchar(255);
alter table contact_details_version
  add constraint contact_details_id_fk
  foreign key (contact_details_id)
  references contact_details(id);

alter table contact_details_version
  add student_username varchar(255);
alter table contact_details_version
  add constraint student_username_fk 
  foreign key (student_username)
  references student(username);

alter table contact_details
  add contact_version_id varchar(255);
alter table contact_details
  add constraint contact_version_id_fk 
  foreign key (contact_version_id)
  references contact_details_version(id);

alter table contact_details
  add student_username varchar(255);
alter table contact_details
  add constraint student_username_fk2
  foreign key (student_username)
  references student(username);

alter table studies
  add student_username varchar(255);
alter table studies
  add constraint student_username_fk3
  foreign key (student_username)
  references student(username);

alter table students_in_course_runs
  add course_run_id varchar(255);
alter table students_in_course_runs
  add constraint course_run_id_fk
  foreign key (course_run_id)
  references course_run(id);

alter table students_in_course_runs
  add studies_id varchar(255);
alter table students_in_course_runs
  add constraint studies_id_fk 
  foreign key (studies_id)
  references studies(id);

alter table course_run
  add semester_id varchar(255);
alter table course_run
  add constraint semester_id_fk
  foreign key (semester_id)
  references semester(id);

alter table course_run
  add course_code varchar(255);
alter table course_run
  add constraint course_code_fk3
  foreign key (course_code)
  references course(course_code);

alter table edux_details
  add course_run_id varchar(255);
alter table edux_details
  add constraint course_run_id_fk2
  foreign key (course_run_id)
  references course_run(id);

alter table course
  add department_id varchar(255);
alter table course
  add constraint department_id_fk 
  foreign key (department_id)
  references department(department_id);

alter table course_details
  add course_code varchar(255);
alter table course_details
  add constraint course_code_fk
  foreign key (course_code)
  references course(course_code);

alter table course_details
  add course_details_version_id_cs varchar(255);
alter table course_details
  add constraint course_details_version_id_cs_fk
  foreign key (course_details_version_id_cs)
  references course_details_version(id);

alter table course_details
  add course_details_version_id_en varchar(255);
alter table course_details
  add constraint course_details_version_id_en_fk
  foreign key (course_details_version_id_en)
  references course_details_version(id);

alter table course_details_version
  add course_code varchar(255);
alter table course_details_version
  add constraint course_code_fk2
  foreign key (course_code)
  references course(course_code);

alter table course_details_version
  add course_details_id varchar(255);
alter table course_details_version
  add constraint course_details_id_fk
  foreign key (course_details_id)
  references course_details(id);

