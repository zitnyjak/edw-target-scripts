create view student_course_list as
select student_username, studies_id, course_run_id, semester_id, course_code
from student, studies, students_in_course_runs, course_run, semester
where student.username = studies.student_username
and studies.id = students_in_course_runs.studies_id
and students_in_course_runs.course_run_id = course_run.id
and course_run.semester_id = semester.id
;
